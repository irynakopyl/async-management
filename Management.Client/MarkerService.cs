﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Management.Common.DTO;

namespace Management.Client
{
    public class MarkerService
    {
        private Timer timer = new Timer();
        private static readonly HttpClient client = new HttpClient();
        public Task<int> MarkRandomTaskWithDelay(int time)
        {
            var tcs = new TaskCompletionSource<int>();
            ElapsedEventHandler handler = null;
            handler = async (o, args) =>
            {
                try
                {
                    timer.Elapsed -= handler;
                    int id = await GetRandomTaskAndSetItFinished();
                    tcs.SetResult(id);
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            };
            timer.Interval = time;
            timer.Elapsed += handler;
            timer.Start();
            return tcs.Task;
        }
        private async Task<int> GetRandomTaskAndSetItFinished()
        {
            int id;
            var result = await client.GetAsync("https://localhost:44305/api/Tasks");
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<List<TaskDTO>>(responseBody);
            Random random = new Random();
            TaskDTO currTask = obj[random.Next(0, obj.Count - 1)];
            currTask.State = TaskStateDTO.Finished;
            id = currTask.Id;
            var serializedTask = JsonConvert.SerializeObject(currTask);
            await client.PutAsync("https://localhost:44305/api/Tasks", new StringContent(serializedTask, Encoding.UTF8, "application/json"));
            return id;
        }
    }
}
