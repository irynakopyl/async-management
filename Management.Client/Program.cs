﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;
using Management.Common.DTO;
namespace Management.Client
{
    class Program
    {
        static readonly HttpClient client = new HttpClient();
        static async Task Main(string[] args)
        {
            string link = "https://localhost:44305/api";
            Console.WriteLine("******* Welcome ! *******\n");
            bool exit = false;
            while (!exit)
            {
                Console.WriteLine("\t\tWhat would you like to do?\t\t\n");
                Console.WriteLine("\t\t1 - Отримати кількість тасків у проекті конкретного користувача");
                Console.WriteLine("\t\t2 - Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків)");
                Console.WriteLine("\t\t3 - Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2020) році для конкретного користувача (по id)");
                Console.WriteLine("\t\t4 - Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.");
                Console.WriteLine("\t\t5 - Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks по довжині name (за спаданням)");
                Console.WriteLine("\t\t6 - Отримати наступну структуру (передати Id користувача в параметри):\n\tUser\n\tОстанній проект користувача(за датою створення)\n\tЗагальна кількість тасків під останнім проектом\n\tЗагальна кількість незавершених або скасованих тасків для користувача\n\tНайтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)");
                Console.WriteLine("\t\t7 - Отримати таку структуру:\n\tПроект\n\tНайдовший таск проекту(за описом)\n\tНайкоротший таск проекту(по імені)\n\tЗагальна кількість користувачів в команді проекту, де або опис проекту > 20 символів, або кількість тасків < 3");
                Console.WriteLine("\t\t8 - Позначити таск готовим");

                Console.WriteLine("\t\t0 - to exit from program ");
                bool num = false;
                int choice = -1;
                while (!num)
                {
                    Console.WriteLine("\t\tPress the number of the action:");
                    num = Int32.TryParse(Console.ReadLine(), out choice);
                    try
                    {
                        switch (choice)
                        {
                            case 0:
                                Console.WriteLine("\t\t-----You are exiting from program");
                                exit = true;
                                break;
                            case 1:
                                //довелось трошки змінити умову і повертати назву проекту, бо Dictionary не хоче парситись( 
                                Console.WriteLine("\t\t-----Введіть id користувача про якого хочете дізнатись: ");
                                int id = int.Parse(Console.ReadLine());
                                GetTaskAmountInProjects(link + "/Users/GetTaskAmountInProjects/" + id, id);
                                break;
                            case 2:
                                Console.WriteLine("\t\t-----Введіть id користувача про якого хочете дізнатись: ");
                                id = int.Parse(Console.ReadLine());
                                await GetAllTasksWithNameLessThan45Chars(link + "/Tasks/GetAllTasksWithNameLessThan45Chars/" + id, id);
                                break;
                            case 3:
                                Console.WriteLine("\t\t-----Введіть id користувача про якого хочете дізнатись: ");
                                id = int.Parse(Console.ReadLine());
                                await GetAllTasksFinishedInCurrentYear(link + "/Users/GetAllTasksFinishedInCurrentYear/" + id, id);
                                break;
                            case 4:
                                await GetTeamsWithUsersOlderThan10(link + "/Teams/GetTeamsWithUsersOlderThan10");
                                break;
                            case 5:
                                await GetAllSortedUsersWithSortedTasks(link + "/Users/GetAllSortedUsersWithSortedTasks");
                                break;
                            case 6:
                                Console.WriteLine("\t\t-----Введіть id користувача про якого хочете дізнатись: ");
                                id = int.Parse(Console.ReadLine());
                                await GetTasksAndProjectsInfo(link + "/Users/GetTasksAndProjectsInfo/" + id, id);
                                break;
                            case 7:
                                await GetAllProjectsAndItsTasksInfo(link + "/Projects/GetAllProjectsAndItsTasksInfo");
                                break;
                            case 8:
                                await Marker();
                                break;
                            default:
                                Console.WriteLine("\t\tYou entered smth unexpected:( ");
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
        private static async void GetTaskAmountInProjects(string link, int id)
        {
            Console.WriteLine("\n--------- Let's check the 1st task! ---------");
            Console.WriteLine($"********** It is user with id {id} *********");
            var result = await client.GetAsync(link);
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            Dictionary<string, int> obj = JsonConvert.DeserializeObject<Dictionary<string, int>>(responseBody);
            foreach (KeyValuePair<string, int> keyValue in obj)
            {
                Console.WriteLine("It is project with name: " + keyValue.Key + " and User has " + keyValue.Value + " tasks in it");
            }
        }
        private static async Task GetAllTasksWithNameLessThan45Chars(string link, int id)
        {
            Console.WriteLine("\n--------- Let's check the 2nd task! ---------");
            Console.WriteLine($"********** It is user with id {id} *********");
            var result = await client.GetAsync(link);
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<List<TaskDTO>>(responseBody);
            foreach (TaskDTO u in obj)
            {
                Console.WriteLine($"I am task for user{id} and I have less than 45 digits in ma name: " + u.Name);
            }
        }
        private static async Task GetAllTasksFinishedInCurrentYear(string link, int id)
        {
            Console.WriteLine("\n--------- Let's check the 3rd task! ---------");
            Console.WriteLine($"********** It is user with id {id} *********");
            var result = await client.GetAsync(link);
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<List<ReadyTaskInCurrentYearDTO>>(responseBody);
            foreach (ReadyTaskInCurrentYearDTO u in obj)
            {
                Console.WriteLine("I am task ready in curr year: " + u.Name);
            }
        }
        private static async Task GetTeamsWithUsersOlderThan10(string link)
        {
            Console.WriteLine("\n--------- Let's check the 4th task! ---------");
            var result = await client.GetAsync(link);
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<List<TeamListDTO>>(responseBody);
            foreach (TeamListDTO u in obj)
            {
                Console.WriteLine("\n It is a team with Id: " + u.Id + " Its Name is: " + u.TeamName + " ---Team Members who are older than 10 are: ");
                foreach (UserDTO l in u.Members)
                {
                    Console.WriteLine("\tMem:  " + l.Id + " --- " + l.FirstName + " --- " + l.RegisteredAt + " Team Id " + l.TeamId);
                }
            }
        }
        private static async Task GetAllSortedUsersWithSortedTasks(string link)
        {
            Console.WriteLine("\n--------- Let's check the 5th task! ---------");
            var result = await client.GetAsync(link);
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<List<TasksListForUserDTO>>(responseBody);
            foreach (TasksListForUserDTO u in obj)
            {
                Console.WriteLine("\n I am a performer of this tasks and my Name is: " + u.Performer.FirstName + "\t" + u.Performer.LastName);
                foreach (TaskDTO t in u.Tasks)
                {
                    Console.WriteLine("\tIt is a task  " + t.Name + "  with Id" + t.Id + " for a user with Id" + t.PerformerId);
                }
            }
        }
        private static async Task GetTasksAndProjectsInfo(string link, int id)
        {
            Console.WriteLine("\n--------- Let's check the 6th task! ---------");
            Console.WriteLine($"********** It is user with id {id} *********");
            var result = await client.GetAsync(link);
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            var uzzer = JsonConvert.DeserializeObject<UserInfoDTO>(responseBody);
            Console.WriteLine("I am user with id: " + uzzer.User.Id + " My last project is: " + uzzer.LastProject.Name + " My longest task is " + uzzer.LongestTask.Name + "I have " + uzzer.NotReadyTasks + " not ready tasks");

        }
        private static async Task GetAllProjectsAndItsTasksInfo(string link)
        {
            Console.WriteLine("\n--------- Let's check the 7th task! ---------");
            var result = await client.GetAsync(link);
            result.EnsureSuccessStatusCode();
            string responseBody = await result.Content.ReadAsStringAsync();
            var obj = JsonConvert.DeserializeObject<List<ProjectInfoDTO>>(responseBody);
            foreach (ProjectInfoDTO u in obj)
            {
                if (u.LongestTask != null && u.ShortestTask != null)
                Console.WriteLine("\n It is a project with id: " + u.Proj.Id + "\t Its longest task is" + u.LongestTask.Name + "\t Its shortest task is:" + u.ShortestTask.Name
                    + "\t Users count:" + u.UserAmount);
            }
        }
        private static async Task Marker()
        {
            MarkerService queries = new MarkerService();
            var markedTaskId = await queries.MarkRandomTaskWithDelay(1000);
            Console.WriteLine("READY TASK: " + markedTaskId);
        }


    }

}

