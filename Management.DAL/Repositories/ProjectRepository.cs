﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ManagementDbContext _dataContext;
        public ProjectRepository(ManagementDbContext context)
        {
            _dataContext = context;
        }

        public async Task<IEnumerable<Project>> GetAll()
        {
            return await Task.Run(() => _dataContext.Projects.AsNoTracking());
        }

        public async Task<Project> Get(int id)
        {
            return await Task.Run(() => _dataContext.Projects.Find(id));
        }

        public async Task Create(Project entity)
        {
            await Task.Run(() => _dataContext.Projects.Add(entity));
        }

        public async Task Update(Project entity)
        {
            await Task.Run(() => _dataContext.Entry(entity).State = EntityState.Modified);
        }

        public async Task<IEnumerable<Project>> Find(Func<Project, Boolean> predicate)
        {
            return await Task.Run(() => _dataContext.Projects.Where(predicate).ToList());
        }

        public async Task Delete(Project entity)
        {
            Project project = await Task.Run(() => _dataContext.Projects.Find(entity.Id));
            if (project != null)
                 await Task.Run(() => _dataContext.Projects.Remove(project));
        }
    }
}
