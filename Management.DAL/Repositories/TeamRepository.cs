﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly ManagementDbContext _dataContext;
        public TeamRepository(ManagementDbContext data)
        {
            _dataContext = data;
        }

        public async Task<IEnumerable<Team>> GetAll()
        {
            return await Task.Run(() => _dataContext.Teams.AsNoTracking()); 
        }

        public async Task<Team> Get(int id)
        {
            return await Task.Run(()=>_dataContext.Teams.Find(id));
        }

        public async Task Create(Team entity)
        {
            await Task.Run(() => _dataContext.Teams.Add(entity));
        }

        public async Task Update(Team entity)
        {
            await Task.Run(() => _dataContext.Entry(entity).State = EntityState.Modified);
        }

        public async Task<IEnumerable<Team>> Find(Func<Team, Boolean> predicate)
        {
            return await Task.Run(() => _dataContext.Teams.Where(predicate).ToList());
        }

        public async Task Delete(Team entity)
        {
            Team team = await Task.Run(() => _dataContext.Teams.Find(entity.Id));
            if (team != null)
                await Task.Run(() => _dataContext.Teams.Remove(team));
        }
    }
}
