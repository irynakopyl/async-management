﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.Common.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDTO user)
        {
            if (ModelState.IsValid)
            {
                return Created("GetById", await _userService.CreateUser(user));
            }
            return NotFound();

        }

        [HttpGet]
        public async Task<ActionResult<ICollection<UserDTO>>> Get()
        {
            return Ok(await _userService.GetUsers());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetById(int id)
        {
            return Ok(await _userService.GetUserById(id));
        }

        [Route("GetTaskAmountInProjects/{id}")]
        public async Task<ActionResult<IDictionary<ProjectDTO, int>>> GetTaskAmountInProjects(int id)
        {
            return Ok(await _userService.GetTaskAmountInProjects(id));
        }

        [Route("GetAllTasksFinishedInCurrentYear/{id}")]
        public async Task<ActionResult<ICollection<ReadyTaskInCurrentYearDTO>>> GetAllTasksFinishedInCurrentYear(int id)
        {
            return Ok(await _userService.GetAllTasksFinishedInCurrentYear(id));
        }

        [Route("GetAllSortedUsersWithSortedTasks")]
        public async Task<ActionResult<ICollection<ReadyTaskInCurrentYearDTO>>> GetAllSortedUsersWithSortedTasks()
        {
            return Ok(await _userService.GetAllSortedUsersWithSortedTasks());
        }

        [Route("GetTasksAndProjectsInfo/{id}")]
        public async Task<ActionResult<UserInfoDTO>> GetTasksAndProjectsInfo(int id)
        {
            return Ok(await _userService.GetTasksAndProjectsInfo(id));
        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> Put([FromBody] UserDTO user)
        {
            await _userService.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _userService.DeleteUser(id);
                return NoContent();
            }
            catch {
                return NotFound(); }
        }
    }
}
