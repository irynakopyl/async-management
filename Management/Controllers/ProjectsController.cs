﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.Common.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {

        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ProjectDTO project)
        {
            if (ModelState.IsValid)
            {
                return Created("Post", await _projectService.CreateProject(project));
            }
            return NotFound();
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> Get()
        {
            return Ok(await _projectService.GetProjects());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetById(int id)
        {
            return Ok(await _projectService.GetProjectById(id));
        }

        [Route("GetAllProjectsAndItsTasksInfo")]
        public async Task<ActionResult<ICollection<ProjectInfoDTO>>> GetAllProjectsAndItsTasksInfo()
        {
            return Ok( await _projectService.GetAllProjectsAndItsTasksInfo());
        }

        [HttpPut]
        public async  Task<ActionResult<ProjectDTO>> Put([FromBody] ProjectDTO project)
        {
            await _projectService.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _projectService.DeleteProject(id);
                return NoContent();
            }
            catch { return NotFound(); }
        }
    }
}
