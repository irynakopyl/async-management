﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.Common.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {

        private readonly TaskService _taskService;

        public TasksController(TaskService teamService)
        {
            _taskService = teamService;
        }
        [HttpPost]
        public async Task< IActionResult> Post([FromBody] TaskDTO task)
        {
            if (ModelState.IsValid)
            {
                return Created("Post",await _taskService.CreateTask(task) );
            }
            return NotFound();
        }

        [HttpGet]
        public async  Task<ActionResult<ICollection<TaskDTO>>> Get()
        {
            return Ok(await _taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDTO>> GetById(int id)
        {
            return Ok(await _taskService.GetTaskById(id));
        }

        [Route("GetAllTasksWithNameLessThan45Chars/{id}")]
        public async Task<ActionResult<ICollection<TeamListDTO>>> GetAllTasksWithNameLessThan45Chars(int id)
        {
            return Ok(await _taskService.GetAllTasksWithNameLessThan45Chars(id));
        }
        [Route("GetNotFinishedTasksForUser/{id}")]
        public async  Task<ActionResult<ICollection<TeamListDTO>>> GetNotFinishedTasksForUser(int id)
        {
            return Ok(await _taskService.GetNotFinishedTasksForUser(id));
        }

        [HttpPut]
        public async Task<ActionResult<TaskDTO>> Put([FromBody] TaskDTO task)
        {
            await _taskService.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task <IActionResult> Delete(int id)
        {
            try
            {
                await _taskService.DeleteTask(id);
                return NoContent();
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
