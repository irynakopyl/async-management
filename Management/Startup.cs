using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Management.DAL.Repositories;
using Management.BLL.Services;
using Management.BLL.MappingProfiles;
using Newtonsoft.Json;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using AutoMapper;
using System.IO;
using Microsoft.EntityFrameworkCore;

namespace Management
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<UserService>();
            services.AddScoped<UserService>();
            services.AddScoped<TaskService>();
            services.AddScoped<ProjectService>();
            services.AddScoped<TeamService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IRepository<Project>, ProjectRepository>();
            services.AddScoped<IRepository<Team>, TeamRepository>();
            services.AddScoped<IRepository<DAL.Models.Taska>, TaskaRepository>();
            services.AddDbContext<ManagementDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"),
                x=>x.MigrationsAssembly("Management.WebAPI")));
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton<IMapper>(mapper);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
