﻿using Management.Common.DTO;
using Management.DAL.Models;
using AutoMapper;

namespace Management.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Taska, TaskDTO>();
            CreateMap<TaskDTO, Taska>();
        }
    }
}
