﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.BLL.Services.Abstract;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Management.BLL.Services
{
    public class TaskService : BaseService
    {
        public TaskService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        { }
        public async Task<TaskDTO> CreateTask(TaskDTO newTask)
        {
            var taskEntity = _mapper.Map<Taska>(newTask);
            await _context.TasksRepo.Create(taskEntity);
            var createdTask = await _context.TasksRepo.Get(taskEntity.Id);
            await _context.SaveAsync();
            return _mapper.Map<TaskDTO>(createdTask);
        }
        public async Task<ICollection<TaskDTO>> GetTasks()
        {
            var tasks = await _context.TasksRepo.GetAll();
            return _mapper.Map<ICollection<TaskDTO>>(tasks.ToList());
        }
        public async Task<TaskDTO> GetTaskById(int id)
        {
            Taska task = await _context.TasksRepo.Get(id);
            return _mapper.Map<TaskDTO>(task);
        }
        public async Task<TaskDTO> UpdateTask(TaskDTO task)
        {
            var result = _mapper.Map<Taska>(task);
            await _context.TasksRepo.Update(result);
            await _context.SaveAsync();
            var reu = await _context.TasksRepo.Get(result.Id);
            return _mapper.Map<TaskDTO>(reu);
        }
        public async Task DeleteTask(int id)
        {
            var taska = await _context.TasksRepo.Get(id);
            if (taska == null) throw new ArgumentException("Invalid id");
            await _context.TasksRepo.Delete(taska);
            await _context.SaveAsync();
        }

        public async Task<ICollection<TaskDTO>> GetAllTasksWithNameLessThan45Chars(int userId)
        {
            var all = await _context.TasksRepo.GetAll();
            return  _mapper.Map<List<TaskDTO>>(all).Where(
                    x => x.PerformerId == userId && x.Name.Length < 45).ToList();
        }
        public async Task<ICollection<TaskDTO>> GetNotFinishedTasksForUser(int userId)
        {
            var all = await _context.TasksRepo.GetAll();
            return  _mapper.Map<List<TaskDTO>>(all)
                .Where(x => x.State != TaskStateDTO.Finished && x.PerformerId == userId).ToList();
        }

    }
}
