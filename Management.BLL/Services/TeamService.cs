﻿using Management.BLL.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Management.BLL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
        }
        public async Task<TeamDTO> CreateTeam(TeamDTO newTeam)
        {
            var teamEntity = _mapper.Map<Team>(newTeam);
            await _context.TeamsRepo.Create(teamEntity);
            await _context.SaveAsync();

            var createdTeam = await _context.TeamsRepo.Get(teamEntity.Id);
            return _mapper.Map<TeamDTO>(createdTeam);
        }
        public async Task<ICollection<TeamDTO>> GetTeams()
        {
            var teams = await _context.TeamsRepo.GetAll();
            return _mapper.Map<ICollection<TeamDTO>>(teams.ToList());
        }
        public async Task<TeamDTO> GetTeamById(int id)
        {
            Team team = await _context.TeamsRepo.Get(id);
            return _mapper.Map<TeamDTO>(team);
        }
        public async Task<TeamDTO> UpdateTeam(TeamDTO team)
        {
            var result = _mapper.Map<Team>(team);
            await _context.TeamsRepo.Update(result);
            await _context.SaveAsync();
            var t = await _context.TeamsRepo.Get(result.Id);
            return _mapper.Map<TeamDTO>(t);
        }
        public async Task DeleteTeam(int id)
        {
            var team = await _context.TeamsRepo.Get(id);
            if (team == null) throw new ArgumentException("Invalid id");
            await _context.TeamsRepo.Delete(team);
            await _context.SaveAsync();
        }
        public async  Task<List<TeamListDTO>> GetTeamsWithUsersOlderThan10()
        {
            var teamsList = await _context.TeamsRepo.GetAll();
            var usersList = await _context.UsersRepo.GetAll();

            var temas = _mapper.Map<List<TeamDTO>>(teamsList);
            var uzzers = _mapper.Map<List<UserDTO>>(usersList);
            return temas
                .GroupJoin(
                uzzers,
                 team => team.Id,
                 user => user.TeamId,
                (t, u) => (
                new TeamListDTO
                {
                    Id = t.Id,
                    TeamName = t.Name,
                    Members = u.Where(x => (2020 - x.Birthday.Year) > 10).OrderByDescending(u => u.RegisteredAt).ToList()
                })).Where(team => team.Members.Any()).ToList();
        }
    }
}
