﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.BLL.Services.Abstract;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Management.BLL.Services
{
    public class ProjectService : BaseService
    {
        public ProjectService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {}
        public async Task<ProjectDTO> CreateProject(ProjectDTO newTeam)
        {
            var projectEntity = _mapper.Map<Project>(newTeam);
            await _context.ProjectsRepo.Create(projectEntity);
            var createdUsr = await _context.ProjectsRepo.Get(projectEntity.Id);
            await _context.SaveAsync();
            return _mapper.Map<ProjectDTO>(createdUsr);
        }
        public async Task<ICollection<ProjectDTO>> GetProjects()
        {
            var projects = await _context.ProjectsRepo.GetAll();
            return _mapper.Map<ICollection<ProjectDTO>>(projects.ToList());
        }
        public async Task<ProjectDTO> GetProjectById(int id)
        {
            Project proj = await _context.ProjectsRepo.Get(id);
            return _mapper.Map<ProjectDTO>(proj);
        }
        public async Task<ProjectDTO> UpdateProject(ProjectDTO task)
        {
            var result = await Task.Run(()=>_mapper.Map<Project>(task));
            await _context.ProjectsRepo.Update(result);
            await _context.SaveAsync();
            var proj = await _context.ProjectsRepo.Get(result.Id);
            return _mapper.Map<ProjectDTO>(proj);
        }
        public async Task DeleteProject(int id)
        {
            var proj = await _context.ProjectsRepo.Get(id);
            if (proj == null) throw new ArgumentException("Invalid id");
            await _context.ProjectsRepo.Delete(proj);
            await _context.SaveAsync();
        }
        public async Task<List<ProjectInfoDTO>> GetAllProjectsAndItsTasksInfo()
        {
            var projList = await _context.ProjectsRepo.GetAll();
            var usersList = await _context.UsersRepo.GetAll();
            var tasksList = await _context.TasksRepo.GetAll();

            var projects = _mapper.Map<ICollection<ProjectDTO>>(projList);
            var users = _mapper.Map<ICollection<UserDTO>>(usersList);
            var tasks = _mapper.Map<ICollection<TaskDTO>>(tasksList);
            return  projects.GroupJoin(users,
                p => p.TeamId,
                u => u.TeamId,
                (pr, us) => new ProjectInfoDTO
                {
                    Proj = pr,
                    LongestTask = tasks.Where(t=>t.ProjectId==pr.Id).OrderByDescending(p => p.Description).FirstOrDefault(),
                    ShortestTask = tasks.Where(t => t.ProjectId == pr.Id).OrderBy(p => p.Name).FirstOrDefault(),
                    UserAmount = us.Count()
                }
                ).ToList();




            /*return _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(p => p.Tasks.Select(t => t.Performer)).Distinct().GroupBy(x => x.TeamId)
               .Select(x => new TeamListDTO
               {
                   Id = x.Key,
                   Members = x.ToList()
               }
               ).Join(
               _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()),
                team => team.Id,
                user => user.TeamId,
               (t, u) =>
               new ProjectInfoDTO
               {
                   Proj = u,
                   LongestTask = u.Tasks.OrderByDescending(p => p.Description).First(),
                   ShortestTask = u.Tasks.OrderBy(p => p.Name).First(),
                   UserAmount = t.Members.Count()
               }).ToList();*/
        }
    }
}
