﻿using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;
using System.Collections.Generic;
using FakeItEasy;
using Management.DAL.Models;
using System.Threading.Tasks;

namespace Management.BLL.Tests
{
    public class TaskServiceTests
    {
        readonly TaskService _taskService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public TaskServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _taskService = new TaskService(_uow, _mapper);
        }
        [Theory]
        [InlineData(1)]
        public async Task GetAllTasksWithNameLessThan45Chars(int Id)
        {

            var check = await _taskService.GetAllTasksWithNameLessThan45Chars(Id);
            Assert.True(check.ToList()[0].Name.Length < 45);
        }
        [Fact]
        public async Task UpdateTaskState_WhenFinished_ThenNewTaskStateIsFinished()
        {
            var task = await _taskService.GetTasks();
            TaskDTO ta = task.FirstOrDefault();
            ta.State = TaskStateDTO.Finished;
            await _taskService.UpdateTask(ta);
            var updTaska = await _taskService.GetTaskById(ta.Id);
            Assert.Equal(TaskStateDTO.Finished, updTaska.State);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(35)]
        public async Task GetNotFinishedTasksForUser_WhenTaskIsPresent_ThenUserIsPerformer(int id)
        {
            var taski = await _taskService.GetNotFinishedTasksForUser(id);
            List<TaskDTO> tasks = taski.ToList();
            Assert.Equal(id, tasks[0].PerformerId);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(35)]
        public async Task GetNotFinishedTasksForUser_WhenWeGotTaskList_ThenFilterOnAllIsCalled(int id)
        {
            ICollection<TaskDTO> tasks = await _taskService.GetNotFinishedTasksForUser(id);
            Assert.NotNull(tasks);

        }
        [Theory]
        [InlineData(1)]
        [InlineData(35)]
        public async Task GetNotFinishedTasksForUser_WhenWeGotTaskList_ThenAllIsNotFinished(int id)
        {
            var notFinished = await _taskService.GetNotFinishedTasksForUser(id);
            List<TaskDTO> tasks = notFinished.ToList();
            Assert.NotEqual(TaskStateDTO.Finished, tasks[0].State);

        }
    }
}
