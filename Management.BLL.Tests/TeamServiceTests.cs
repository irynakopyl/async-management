﻿using System;
using Xunit;
using Management.BLL.Services;
using AutoMapper;
using Management.DAL.Interfaces;
using Management.BLL.MappingProfiles;
using Management.DAL.Repositories;
using Management.Common.DTO;
using System.Linq;
using System.Threading.Tasks;

namespace Management.BLL.Tests
{
    public class TeamServiceTests
    {
        readonly TeamService _teamService;
        readonly IMapper _mapper;
        readonly IUnitOfWork _uow;
        public TeamServiceTests()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            var factory = new ConnectionFactory();
            _mapper = mappingConfig.CreateMapper();
            _uow = new UnitOfWork(factory.CreateContextForInMemory());
            _teamService = new TeamService(_uow, _mapper);
        }
        [Fact]
        public async Task CreateTeam_WhenCreated_ThenCountPlusOne()
        {
            var teams = await _teamService.GetTeams();
            int amount = teams.Count;
            var team = new TeamDTO
            {
                Name = "NewTeam",
                CreatedAt = DateTime.Now,
            };
            var newTeam = await _teamService.CreateTeam(team);
            var result = await _teamService.GetTeams();
            //Assert.NotNull(newTeam);
            Assert.Equal(amount + 1,result.Count);
        }
        [Fact]
        public async Task GetTeamsWithUsersOlderThan10()
        {
            var teams = await _teamService.GetTeamsWithUsersOlderThan10();
            bool check = (DateTime.Now.Year - teams.FirstOrDefault().Members.FirstOrDefault().Birthday.Year) > 10;
            Assert.True(check);
        }
    }
}
